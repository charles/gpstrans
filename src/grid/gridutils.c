/****************************************************************************/
/*                                                                          */
/* ./grid/gridutils.c   -   Calculate datum variables                       */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"
#include "Prefs.h"


/****************************************************************************/
/* Calculate datum parameters.                                              */
/****************************************************************************/
void
datumParams (short datum, double *a, double *es)
{
  extern struct DATUM const gDatum[];
  extern struct ELLIPSOID const gEllipsoid[];


  double f = 1.0 / gEllipsoid[gDatum[datum].ellipsoid].invf;	/* flattening */

  *es = 2 * f - f * f;		/* eccentricity^2 */
  *a = gEllipsoid[gDatum[datum].ellipsoid].a;	/* semimajor axis */
}
