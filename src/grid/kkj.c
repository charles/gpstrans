/****************************************************************************/
/*                                                                          */
/* ./grid/kkj.c   -   Convert to and from KKJ Grid Format                   */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*    Copyright (c) 1996 by Janne Sinkkonen (janne@iki.fi)                  */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"
#include <math.h>


/* define constants */
static const double lat0 = 0.0;	/* reference transverse mercator latitude */
static const double lon0 = 27.0;
static const double k0 = 1.0;


/****************************************************************************/
/* Convert degree to KKJ Grid.                                              */
/****************************************************************************/
void
DegToKKJ (double lat, double lon, char *zone, double *x, double *y)
{
  sprintf (zone, "27E");
  toTM (lat, lon, lat0, lon0, k0, x, y);

  /* false easting */
  *x = *x + 500000.0;
}


/****************************************************************************/
/* Convert KKJ Grid to degree.                                              */
/****************************************************************************/
void
KKJtoDeg (short zone, short southernHemisphere, double x, double y,
	  double *lat, double *lon)
{

  x -= 500000.0;
  fromTM (x, y, lat0, lon0, k0, lat, lon);
}
