/****************************************************************************/
/*                                                                          */
/* ./grid/gkk.c   -   Convert to and from GKK Grid Format                   */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*    Copyright (c) 1996 by Janne Sinkkonen (janne@iki.fi)                  */
/*    Copyright (c) 2000 by Andreas Lange   (andreas.lange@rhein-main.de)   */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"
#include <math.h>

/* NOTE: Gauss-Krueger refers to a grid system as well as to a
   projection method. Do not confuse both. 

   germany/europe:
   Mittelmeridian 0�  = Zone 0 : 0500000 -1.5 -  0 -  1.5 
   Mittelmeridian 3�  = Zone 1 : 1500000  1.5 -  3 -  4.5
   Mittelmeridian 6�  = Zone 2 : 2500000  4.5 -  6 -  7.5
   Mittelmeridian 9�  = Zone 3 : 3500000  7.5 -  9 - 10.5
   Mittelmeridian 12� = Zone 4 : 4500000 10.5 - 12 - 13.5
   Mittelmeridian 15� = Zone 5 : 5500000 13.5 - 15 - 16.5
   
   General Gauss-Krueger:
   3� wide meridian zones
   120 zones from 0� to 360�
   False Easting of 500000 m 
   Normally not defined on southern hemisphere. 
*/

/* define constants */
/* divider for false easting part */
#define DIVIDER 1000000.0
/* false easting without zone number prefix */
#define FEAST   500000.0
/* width of the meridian longitude zones */
#define MWIDTH       3.0

static const double lat0 = 0.0;	/* reference transverse mercator latitude */
static double lon0 = 0.0;	/* reference transverse mercator longitude */
static const double k0 = 1.0;	/* scale factor at central meridian */


/****************************************************************************/
/* Convert degree to GKK Grid.                                              */
/****************************************************************************/
void
DegToGKK (double lat, double lon, double *x, double *y)
{
  int zonenbr;

  /* not defined on southern hemisphere,
     only defined for middle european longitudes,
     that is zone 0 to 5. */
  if (lat < 0.0 || lon < 0.0 || lon > 16.5)
    {
      *x = 0.0;
      *y = 0.0;
      return;
    }

  /* calculate zone */
  zonenbr = floor ((lon / (double) MWIDTH) + (double) 0.5);

  /* set lon0 to reference meridian/longitude */
  lon0 = (double) (zonenbr * MWIDTH);

  /* projection to tm */
  toTM (lat, lon, lat0, lon0, k0, x, y);

  /* add false easting */
  *x += (zonenbr * DIVIDER) + FEAST;
}


/****************************************************************************/
/* Convert GKK Grid to degree.                                              */
/****************************************************************************/
void
GKKtoDeg (double x, double y, double *lat, double *lon)
{

  /* do some sanity checks */
  if (x > 5999999.0 || x < 0.0 || y < 0.0 || y > 9999999.0)
    {
      *lat = 0;
      *lon = 0;
      return;
    }

  /* set lon0 to reference meridian/longitude */
  lon0 = (floor (x / DIVIDER)) * MWIDTH;

  /* subtract false easting */
  x -= (((floor (x / DIVIDER)) * DIVIDER) + FEAST);

  /* inverse projection from tm */
  fromTM (x, y, lat0, lon0, k0, lat, lon);
}
