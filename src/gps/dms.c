/****************************************************************************/
/*                                                                          */
/* ./gps/dms.c   -   Convert to various position formats                    */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"



/****************************************************************************/
/* Convert degrees to dd mm'ss.s" (DMS-Format)                              */
/****************************************************************************/
char *
toDMS (double a)
{
  short neg = 0;
  double d, m, s;
  static char dms[20];

  if (a < 0.0)
    {
      a = -a;
      neg = 1;
    }
  d = (double) ((int) a);
  a = (a - d) * 60.0;
  m = (double) ((int) a);
  s = (a - m) * 60.0;

  if (s > 59.5)
    {
      s = 0.0;
      m += 1.0;
    }
  if (m > 59.5)
    {
      m = 0.0;
      d += 1.0;
    }
  if (neg)
    d = -d;

  sprintf (dms, "%.0f�%02.0f'%04.1f\"", d, m, s);
  return dms;
}


/****************************************************************************/
/* Convert dd mm'ss.s" (DMS-Format) to degrees.                             */
/****************************************************************************/
double
DMStoDegrees (char *dms)
{
  int d, m;
  double s;

  sscanf (dms, "%d%d%lf", &d, &m, &s);
  s = (double) (abs (d)) + ((double) m + s / 60.0) / 60.0;

  if (d >= 0)
    return s;
  else
    return -s;
}


/****************************************************************************/
/* Convert degrees to dd mm.mmm' (DMM-Format)                               */
/****************************************************************************/
char *
toDM (double a)
{
  short neg = 0;
  double d, m;
  static char dm[13];

  if (a < 0.0)
    {
      a = -a;
      neg = 1;
    }

  d = (double) ((int) a);
  m = (a - d) * 60.0;

  if (m > 59.5)
    {
      m = 0.0;
      d += 1.0;
    }
  if (neg)
    d = -d;

  sprintf (dm, "%.0f�%06.3f'", d, m);
  return dm;
}


/****************************************************************************/
/* Convert dd mm.mmm' (DMM-Format) to degree.                               */
/****************************************************************************/
double
DMtoDegrees (char *dms)
{
  int d;
  double m;

  sscanf (dms, "%d%lf", &d, &m);
  m = (double) (abs (d)) + m / 60.0;

  if (d >= 0)
    return m;
  else
    return -m;
}
