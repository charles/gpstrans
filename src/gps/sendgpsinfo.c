/****************************************************************************/
/*                                                                          */
/* ./gps/sendgpsinfo.c   -   Send data to GPS                               */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*    Copyright (c) 1998 by Matthias Kattanek (mattes@ugraf.com)            */
/*	980802	sscanf modification %d -> %hd				    */
/*		in sendGPSInfo() send one record too much 		    */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/

#include "defs.h"
#include "Garmin.h"
#include "Prefs.h"
#include "protocols.h"
#include <stdlib.h>
#include <time.h>
#include <ctype.h>

/* define external variables */
extern BYTE gGarminMessage[MAX_LENGTH];
extern struct PREFS gPrefs;
extern char *protocols;

/* define global variables */
static char fileData[MAX_LINE];
static BYTE newTrack;
static int track_count;
static BYTE message[520];	/* allow for many 0x10 bytes which must be doubled */
struct PREFS gFilePrefs;

/* additions for mXmap */
void mxmap_route (char *, int);
char *mystrsep2 (char **stringp, char *delim);
void strconvert (char *);

/* prototype functions */
static short records (FILE *ifile, short type);
static short doWaypoint (short type);
static short doTrack (void);
static short doRoute (void);
static short doAlmanac (void);
static short sendRecords (short);
static void copyNumber (BYTE * p, long n);
static void copyFloat (BYTE * p, double d);
static char *field (char *a, short n);
static void cpystr (BYTE * p, char *q, short n);
static short getFileFormat (char *, char*);
static void cleanupDMS (char *s);


struct
{
  int count;
  int type;
  int name;
  int comment;
  int date;
  int altitude;
  int position;			/* first column of position data */
} column;


/****************************************************************************/
/* Send "commit-suicide-command" to GPS.                                    */
/****************************************************************************/
long
sendGPSOff ()
{
  extern struct PREFS gPrefs;
  extern char gMessageStr[];
  short err;

  /* First check if GPS responds to the "are-you-ready" package */
  if (!CheckGPS ())
    NotResponding ();

  /* open serial device */
  if ((err = serialOpen (GARMIN)) != noErr)
    {
      sprintf (gMessageStr, "The port initialization of %s has failed.",
	       gPrefs.Device);
      Error (gMessageStr);
      return 99;		/* Currently Error never returns.
				   Allow for another
				   implementation? */
    }

  /* send command - no respond is send from GPS */
  sendGPSMessage (off1, 4);

  /* close serial device */
  serialClose ();

  printf ("GPS turned off successfully\n");

  return 0;
}


/****************************************************************************/
/* Send multiple line packages to GPS (route, waypoint, track)              */
/****************************************************************************/
void
sendGPSInfo (FILE *ifile, short type)
{
  extern char gMessageStr[];

  short recs, processed = 0;
  short err, result = 1;
  char *rType;
  BYTE *term;
  char secondLine[MAX_LINE];
  int junk;
  char *junk_str;

  /* First check it GPS responds to the "are-you-ready" package */
  if (!CheckGPS ())
    NotResponding ();

  junk=getGPSVersion(&junk_str); /* load the "protocols" array */

  /* open serial device */
  if ((err = serialOpen (GARMIN)) != noErr)
    {
      sprintf (gMessageStr, "The port initialization of %s has failed.",
	       gPrefs.Device);
      Error (gMessageStr);
      return;
    }

  recs = records (ifile, type);
  recs--;			/* before vers0.34 sent one record too many*/

  /* Continue only if GPS responds to the send-request package */
  if (sendRecords (recs) == 0)
    {
      Error ("GPS did not respond to send-request packet.\n");
      return;
    }

  /* return to beginning of file and read first lines */
  rewind (ifile);
  GetLine (ifile, fileData, 1);
  GetLine (ifile, secondLine, 1); /* some files have two header lines */
  rewind (ifile);		/* reading header lines again is harmless */

  if (!getFileFormat (fileData, secondLine))
    {
      Error ("The file header format is incorrect.");
      return;
    }

  naks = 0;
  InitBarGraph ();

  /* process different requests */
  switch (type)
    {
    default:
    case ALMANAC:
      rType = "almanac";
      term = almt;
      while (GetLine (ifile, fileData, 0) &&
	     processed < recs && result)
	{
	  if (fileData[0] == 'A')
	    {
	      result = doAlmanac ();
	      SetBarGraph ((double) (++processed) / (double) recs);
	    }
	}
      break;

    case ROUTE:
      rType = "routes";
      term = rtet;

      if (file_format != MAYKO)
	{
	  while (GetLine (ifile, fileData, 0) &&
		 processed < recs && result)
	    {
	      if (fileData[0] == 'R')
		{
		  result = doRoute (); /* send route header */
		  ++processed;
		}
	      if (fileData[0] == 'W')
		{
		  result = doWaypoint (ROUTE);
		  ++processed;
		}
	      SetBarGraph ((double) (processed) / (double) recs);
	    }
	  break;


	}
      else
	{		// manage Mayko mXmap format
	  //      case MXMAP_ROUTE:

	  sprintf (fileData, "R\t0\tMAYKO MXMAP\n");
	  result = doRoute ();

	  while (GetLine (ifile, fileData, 0) && result)
	    {
	      if (*fileData == '\0')
		break;
	      mxmap_route (fileData, processed);
	      //        printf("mxmap-wp %s-\n", fileData );
	      //        printf("%s-\n", fileData );
	      if (fileData[0] == 'W')
		{
		  result = doWaypoint (ROUTE);
		  ++processed;
		}
	      *fileData = '\0';
	      SetBarGraph ((double) (processed) / (double) recs);
	    }
	  recs = processed;
	  break;
	}

    case TRACK:
      rType = "track";
      term = trkt;
      track_count = newTrack = 1;
      while (GetLine (ifile, fileData, 0) &&
	     processed < recs && result)
	{
	  if (fileData[0] == 'T')
	    {
	      result = doTrack ();
	      SetBarGraph ((double) (++processed) / (double) recs);
	    }
	  else
	    {
	      newTrack = 1;
	      track_count++;
	    }
	}
      break;

    case WAYPOINT:
      rType = "waypoint";
      term = wptt;
      while (GetLine (ifile, fileData, 0) &&
	     (processed < recs) && result)
	{
	  if (fileData[0] == 'W')
	    {
	      result = doWaypoint (WAYPOINT);
	      SetBarGraph ((double) (++processed) / (double) recs);
	    }
	}
      break;
    }

  /* close BarGraph window */
  CloseBarGraph ();

  /* send terminator message to tell GPS - its over */
  if (result)
    {
      sendGPSMessage (term, 4);

      /* get final response but ignore it */
      getGPSMessage ();

      /* print number of packages transferred */
      sprintf (gMessageStr,
	       "%d %s packets were successfully transferred to the GPS receiver.",
	       recs-naks, rType);
      Message (gMessageStr);
      if (naks)
	{
	  sprintf (gMessageStr,
		   "%d packets were rejected.\n", naks);
	  Error (gMessageStr);
	}
      if (verbose && track_count)
	printf("%d tracks were transferred.\n", track_count);
    }

  /* close serial device */
  serialClose ();
}


/*
 * Take mXmap route records and changes it back into the format 
 * that gpstrans is using. Not the ideal solution,
 * actually pretty nasty hack, but works though.
 */
void
mxmap_route (char *Datastring, int mrec)
{
  static char *month[12] = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
    "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
  };
  int i;
  char *p, *r;
  char xname[80];
  char xlat[10], xlon[10];
  time_t t;
  struct tm *ts;

  time (&t);
  ts = localtime (&t);

  sprintf (xname, "MX%02d", mrec + 1);	// set dummy name
  p = Datastring;
  r = mystrsep2 (&p, ",\n");
  if (r != NULL)
    {
      strcpy (xname, r);
      strconvert (xname);	// convert to upper, strip spaces
    }

  r = mystrsep2 (&p, ",\n");
  strcpy (xlat, r);
  r = mystrsep2 (&p, ",\n");
  strcpy (xlon, r);

// this is how a gpstrans route record looks like
//printf("W\t123456\t123456789012345678901234527-JUL-98 23:56\t12/31/1989 -8:00:00\t42.3753083\t-71.0194016\n");

  sprintf (Datastring, "W\t%s\t", xname);

// this seems not to be necessary to have doRoute() understand the record
//      i = strlen( Datastring );
//      p = Datastring + i;
//      i = 32 - i;
//      while ( i-- )
//          *p++ = '.';

  i = strlen (Datastring);
  sprintf (Datastring + i, "%2d-%s-%02d %02d:%02d\t",
	   ts->tm_mday,
	   month[ts->tm_mon], ts->tm_year, ts->tm_hour, ts->tm_min);

  i = strlen (Datastring);
  sprintf (Datastring + i, "12/31/1989 -8:00:00\t%s\t%s\n", xlat, xlon);

}

char *
mystrsep2 (char **stringp, char *delim)
{
  char *begin, *end;

  begin = *stringp + strspn (*stringp, delim);
  end = *stringp + strcspn (*stringp, delim);

  if (end == *stringp)
    begin = NULL;

  if (*end != '\0')
    *end++ = '\0';
  *stringp = end;

  return begin;
}

void
strconvert (char *p)
{
  char *dest;

  dest = p;
  while (*p)
    {
      if (!isspace (*p))
	{
	  if (islower (*p))
	    *dest = toupper (*p);
	  else
	    *dest = *p;
	  dest++;
	}
      p++;
    }
  *dest = '\0';

}

/****************************************************************************/
/* Waypoint file format:                                                    */
/*                                                                          */
/*                                                                          */
/* Format: DDD  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* W    HOME  123 SNOWMASS  07/28/1994 20:12:54  40.1777471  -105.0892654   */
/* 0    1     2             3                    4           5              */
/* TYPE NAME  COMMENT       DATE                 LATITUDE    LONGITUDE      */
/*                                                                          */
/*                                                                          */
/* Format: DMM  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* W    HOME  123 SNOWMASS  07/28/1994 20:12:54  40 10.665'  -105 05.356'   */
/* 0    1     2             3                    4            5             */
/* TYPE NAME  COMMENT       DATE                 LATITUDE     LONGITUDE     */
/*                                                                          */
/*                                                                          */
/* Format: DMS  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* W    HOME  123 SNOWMASS  07/28/1994 20:12:54  40 10'39.9" -105 05'21.4"  */
/* 0    1     2             3                    4           5              */
/* TYPE NAME  COMMENT       DATE                 LATITUDE    LONGITUDE      */
/*                                                                          */
/*                                                                          */
/* Format: UTM  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* W    HOME  123 SNOWMASS  07/28/1994 20:12:54  13 T  0492400  4447279     */
/* 0    1     2             3                    4  5  6        7           */
/* TYPE NAME  COMMENT       DATE                 ZE ZN EASTING  NORTHING    */
/*                                                                          */
/*                                                                          */
/* Format: BNG  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* W    EXAMPL BRITISH GRD  07/28/1994 20:12:54  SE   12345    67890        */
/* 0    1      2            3                    4    5        6            */
/* TYPE NAME   COMMENT      DATE                 ZONE EASTING  NORTHING     */
/*                                                                          */
/*                                                                          */
/* Format: ITM  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* W    EXAMPL IRISH GRID   07/28/1994 20:12:54  IN   12345    67890        */
/* 0    1      2            3                    4    5        6            */
/* TYPE NAME   COMMENT      DATE                 ZONE EASTING  NORTHING     */
/*                                                                          */
/*                                                                          */
/* Format: FIN  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* W    EXAMPL SWISS GRID   07/28/1994 20:12:54  27E 012345  067890         */
/* 0    1      2            3                    4   5       6              */
/* TYPE NAME   COMMENT      DATE                 ZE  EASTING NORTHING       */
/*                                                                          */
/*                                                                          */
/* Format: SUI  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* W    EXAMPL SWISS GRID   07/28/1994 20:12:54  012345  067890             */
/* 0    1      2            3                    5       6                  */
/* TYPE NAME   COMMENT      DATE                 EASTING NORTHING           */
/*                                                                          */
/*                                                                          */
/* Format: SEG  UTC Offset:   1.00 hrs  Datum[085]: RT 90                   */
/* W    EXAMPL SWEDISH GRID 07/28/1994 20:12:54  012345  067890             */
/* 0    1      2            3                    4       5                  */
/* TYPE NAME   COMMENT      DATE                 EASTING NORTHING           */
/*                                                                          */
/*                                                                          */
/* Format: GKK  UTC Offset:   1.00 hrs  Datum[102]: Potsdam                 */
/* W    EXAMPL GERMAN GRID  07/28/1994 20:12:54  0123456  0678901           */
/* 0    1      2            3                    4        5                 */
/* TYPE NAME   COMMENT      DATE                 EASTING  NORTHING          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/

/****************************************************************************/
/* Process Waypoint package - return 0 if GPS does not respond.             */
/****************************************************************************/
static short
doWaypoint (short type)
{
  double x, y;
  short zone;
  char finzone[5];		/* Currently always 27E, i.e. not used */
  char bgzone[5];
  char nsZone;
  int pos;
  char *s;

  enum { LAT, LON };
  enum { ZE, ZN, EASTING, NORTHING }; /* UTM Grid */
  enum { BGZ, BGEAST, BGNORTH }; /* British, Irish Grid */
  enum { SEAST, SNORTH };	/* Swiss Grid */
  enum { SEGEAST, SEGNORTH };	/* Swedish Grid */
				/* same format for German Grid */
  struct 
  {
    char type;
    char *comment, *name;    
    long seconds;
    double latitude, longitude;
    double altitude;
    struct {
      /* latitude and longitude are always present */
      unsigned altitude:1;
      unsigned seconds:1;
      unsigned name:1;
      unsigned comment:1;
    } has;
  } pkt;
  char empty[]="";

  pkt.has.altitude =
    pkt.has.seconds =
    pkt.has.name =
    pkt.has.comment = 0;

  pkt.altitude = 1.01e24;
  pkt.seconds = ~0;
  pkt.comment = pkt.name = empty;

  if (column.type >= 0) 
    pkt.type = *field(fileData, column.type);

  if (column.date >= 0)
    {
      char date[20];
      strncpy (date, field (fileData, column.date), 19);
      date[19] = 0;
      pkt.seconds = dt2secs (date, gFilePrefs.offset);
      pkt.has.seconds = 1;
    }
  if ((column.altitude >= 0) && 
      (!strstr(field(fileData, column.altitude), "unknown")))
    {
      pkt.altitude = atof (field (fileData, column.altitude));
      pkt.has.altitude = 1;
    }

  if (column.name >= 0) 
    {
      pkt.name = field(fileData, column.name);
      pkt.has.name = 1;
    }

  if (column.comment >= 0) 
    {
      pkt.comment = field(fileData, column.comment);
      pkt.has.comment = 1;
    }

  pos=column.position;

  /* handle various grids */
  switch (gFilePrefs.format)
    {
    case DMS:
      cleanupDMS (fileData);
      pkt.latitude = DMStoDegrees (field (fileData, pos+LAT));
      pkt.longitude = DMStoDegrees (field (fileData, pos+LON));
      break;

    case DMM:
      cleanupDMS (fileData);
      pkt.latitude = DMtoDegrees (field (fileData, pos+LAT));
      pkt.longitude = DMtoDegrees (field (fileData, pos+LON));
      break;

    case DDD:
      sscanf (field (fileData, pos+LAT), "%lf %lf", &pkt.latitude, &pkt.longitude);
      break;

    case UTM:			/* UTM Grid */
      sscanf (field (fileData, pos+ZE), "%hd", &zone);
      sscanf (field (fileData, pos+ZN), "%c", &nsZone);
      sscanf (field (fileData, pos+EASTING), "%lf %lf", &x, &y);
      UTMtoDeg (zone, nsZone <= 'M', x, y, &pkt.latitude, &pkt.longitude);
      break;

    case KKJ:			/* KKJ Grid */
      sscanf (field (fileData, pos+ZE), "%s", finzone);
      sscanf (field (fileData, pos+EASTING), "%lf %lf", &x, &y);
      KKJtoDeg (2, 0, x, y, &pkt.latitude, &pkt.longitude);	/* 2 is 27E, not used currently */
      break;

    case BNG:			/* British Grid */
      sscanf (field (fileData, pos+BGZ), "%s", bgzone);
      sscanf (field (fileData, pos+BGEAST), "%lf %lf", &x, &y);
      BNGtoDeg (bgzone, x, y, &pkt.latitude, &pkt.longitude);
      break;

    case ITM:			/* Irish Grid */
      sscanf (field (fileData, pos+BGZ), "%s", bgzone);
      sscanf (field (fileData, pos+BGEAST), "%lf %lf", &x, &y);
      ITMtoDeg (bgzone, x, y, &pkt.latitude, &pkt.longitude);
      break;

    case SEG:			/* Swedish Grid */
      sscanf (field (fileData, pos+SEGEAST), "%lf %lf", &x, &y);
      SEGtoDeg (x, y, &pkt.latitude, &pkt.longitude);
      break;

    case GKK:			/* German Grid */
      sscanf (field (fileData, pos+SEGEAST), "%lf %lf", &x, &y);
      GKKtoDeg (x, y, &pkt.latitude, &pkt.longitude);
      break;

    default:
      break;
    }

  /* translate to selected datum */
  translate (0, &pkt.latitude, &pkt.longitude, gFilePrefs.datum);

  if (column.name >= 0) 
    {				/* eliminate trailing tab and spaces
				   (destructive, so must follow last
				   use of "field" function */
      if ((s = strstr(pkt.name, "\t")))
	*s = 0;			/* replace tab with null */
      s = pkt.name;
      s += strlen(s);
      if (*s == '\n')
	*s = 0;			/* replace newline with null */
      for ( ; (--s > pkt.name) && (*s == ' '); )
	*s = 0;			/* eliminate trailing blanks */
    }

  if (column.comment >= 0) 
    {				/* eliminate trailing tab and spaces
				   (destructive, so must follow last
				   use of "field" function */
      if ((s = strstr(pkt.comment, "\t")))
	*s = 0;			/* replace tab with null */
      s = pkt.comment;
      s += strlen(s);
      if (*s == '\n')
	*s = 0;			/* replace newline with null */
      for ( ; (--s > pkt.comment) && (*s == ' '); )
	*s = 0;			/* eliminate trailing blanks */
    }

  if (debugging>1)
    {
      printf("pkt: name=\"%s\"  comment=\"%s\"\n", pkt.name, pkt.comment);
      if(pkt.has.seconds) printf("%s", ctime(&pkt.seconds));
      printf("latitude=%9.6f longitude=%9.6f altitude=%5.0f\n",
	     pkt.latitude, pkt.longitude, pkt.altitude);
    }

  /* now create the appropriate kind of packet for this device */

  /* create package header */
  if (type == WAYPOINT)
    message[0] = Pid_Wpt_Data;
  else				/* type == ROUTE */
    message[0] = Pid_Rte_Wpt_Data;

  if (strstr(protocols, "D100"))
    {
      D100_Wpt_Type *d=(D100_Wpt_Type *)(message+2);
      
      /* number of message bytes to follow */
      message[1] = sizeof(*d);

      /* copy position and time to GPS package */
      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      copyNumber ((BYTE *)&d->unused, pkt.seconds);
      cpystr ((unsigned char *)&d->ident, pkt.name, 6);
      cpystr ((unsigned char *)&d->cmnt, pkt.comment, 40);
    }
  else if (strstr(protocols, "D101"))
    {
      D101_Wpt_Type *d=(D101_Wpt_Type *)(message+2);
      
      /* number of message bytes to follow */
      message[1] = sizeof(*d);

      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      copyNumber ((BYTE *)&d->unused, pkt.seconds);
      cpystr ((unsigned char *)&d->ident, pkt.name, 6);
      cpystr ((unsigned char *)&d->cmnt, pkt.comment, 40);
    }
  else if (strstr(protocols, "D102"))
    {
      D102_Wpt_Type *d=(D102_Wpt_Type *)(message+2);
      
      /* number of message bytes to follow */
      message[1] = sizeof(*d);

      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      copyNumber ((BYTE *)&d->unused, pkt.seconds);
      cpystr ((unsigned char *)&d->ident, pkt.name, 6);
      cpystr ((unsigned char *)&d->cmnt, pkt.comment, 40);
    }
  else if (strstr(protocols, "D103"))
    {
      D103_Wpt_Type *d=(D103_Wpt_Type *)(message+2);
      
      /* number of message bytes to follow */
      message[1] = sizeof(*d);

      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      copyNumber ((BYTE *)&d->unused, pkt.seconds);
      cpystr ((unsigned char *)&d->ident, pkt.name, 6);
      cpystr ((unsigned char *)&d->cmnt, pkt.comment, 40);
    }
  else if (strstr(protocols, "D104"))
    {
      D104_Wpt_Type *d=(D104_Wpt_Type *)(message+2);
      
      /* number of message bytes to follow */
      message[1] = sizeof(*d);

      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      copyNumber ((BYTE *)&d->unused, pkt.seconds);
      cpystr ((unsigned char *)&d->ident, pkt.name, 6);
      cpystr ((unsigned char *)&d->cmnt, pkt.comment, 40);
    }
  else if (strstr(protocols, "D105"))
    {
      D105_Wpt_Type *d=(D105_Wpt_Type *)(message+2);
      
      /* number of message bytes to follow */
      message[1] = sizeof(*d);

      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      /* no date field */
      cpystr ((unsigned char *)&d->ident, pkt.name, 15);
      /* no comment field */
    }
  else if (strstr(protocols, "D106"))
    {
      D106_Wpt_Type *d=(D106_Wpt_Type *)(message+2);
      
      /* number of message bytes to follow */
      message[1] = sizeof(*d);

      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      /* no date field */
      cpystr ((unsigned char *)&d->ident, pkt.name, 15);
      /* no comment field */
    }
  else if (strstr(protocols, "D107"))
    {
      D107_Wpt_Type *d=(D107_Wpt_Type *)(message+2);
      
      /* number of message bytes to follow */
      message[1] = sizeof(*d);

      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      copyNumber ((BYTE *)&d->unused, pkt.seconds);
      cpystr ((unsigned char *)&d->ident, pkt.name, 6);
      cpystr ((unsigned char *)&d->cmnt, pkt.comment, 40);
    }
  else if (strstr(protocols, "D108"))
    {
      D108_Wpt_Type *d=(D108_Wpt_Type *)(message+2);
      
      copyFloat ((BYTE *)&d->alt, pkt.altitude);
      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      /* no date field */
      s = (char *)&d->ident;
      strcpy (s, pkt.name);
      s += strlen(s)+1;
      strcpy (s, pkt.comment);
      s += strlen(s)+1;
      message[1] = s- (char *)d; /* number of message bytes */
    }
  else if (strstr(protocols, "D109"))
    {
      D109_Wpt_Type *d=(D109_Wpt_Type *)(message+2);

      copyFloat ((BYTE *)&d->alt, pkt.altitude);
      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      /* no date field */
      s = (char *)&d->ident;
      strcpy (s, pkt.name);
      s += strlen(s)+1;
      strcpy (s, pkt.comment);
      s += strlen(s)+1;
      message[1] = s- (char *)d; /* number of message bytes */
    }
  else if (strstr(protocols, "D110"))
    {
      D110_Wpt_Type *d=(D110_Wpt_Type *)(message+2);

      copyFloat ((BYTE *)&d->alt, pkt.altitude);
      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      copyNumber ((BYTE *)&d->time, pkt.seconds);
      s = (char *)&d->ident;
      strcpy (s, pkt.name);
      s += strlen(s)+1;
      strcpy (s, pkt.comment);
      s += strlen(s)+1;
      message[1] = s- (char *)d; /* number of message bytes */
    }


  /* send package to GPS */
  sendGPSMessage (message, message[1]+2);

  /* make sure GPS responds */
  return getGPSack ();
}


static int
colorNameToCode (char *name, char *protocol)
{
  char *color_name[18]={
    "black",
    "dark red",
    "dark green",
    "dark yellow",
    "dark blue",
    "dark magenta",
    "dark cyan",
    "light gray",
    "dark gray",
    "red",
    "green",
    "yellow",
    "blue",
    "magenta",
    "cyan",
    "white",
    "transparent",
    "default"
  };
  int i;

  for (i=0; i<18; i++)
    if (strcasecmp(name, color_name[i]) == 0)
      break;
  if (i == 18)
    i = 0;			/* unrecognized color name */
  if (i == 17)
    {
      if ((strcasecmp(protocol,"D108")==0) ||
	  (strcasecmp(protocol,"D310")==0))
	i = 255;
      else
	i = 0x1f;		/* D109 */
    }
  return i;
}

/****************************************************************************/
/* Track file format:                                                       */
/*                                                                          */
/*                                                                          */
/* T    08/29/1994 13:32:01  40.146967 -105.125464 13   T  489391  4443614  */
/* 0    1                    2          3          4    5  6       7        */
/* TYPE DATE                 LAT        LON        ZONE ZN EASTING NORTHING */
/*                                                                          */
/*                                                                          */
/* Format: DDD  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* T    09/09/1994 20:03:49  40.1780207 -105.0901666                        */
/* 0    1                    2          3                                   */
/* TYPE DATE/TIME            LATITUDE   LONGITUDE                           */
/*                                                                          */
/*                                                                          */
/* Format: DMM  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* T    09/09/1994 20:03:49  40 10.681'  -105 05.410'                       */
/* 0    1                    2           3                                  */
/* TYPE DATE/TIME            LATITUDE    LONGITUDE                          */
/*                                                                          */
/*                                                                          */
/* Format: DMS  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* T    09/09/1994 20:03:49  40 10'40.9"  -105 05'24.6"                     */
/* 0    1                    2            3                                 */
/* TYPE DATE/TIME            LATITUDE     LONGITUDE                         */
/*                                                                          */
/*                                                                          */
/* Format: UTM  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* T    09/09/1994 20:03:49  13  T   0492323   4447310                      */
/* 0    1	             2   3   4         5                            */
/* TYPE DATE/TIME            ZE  ZN  EASTING   NORTHING                     */
/*                                                                          */
/*                                                                          */
/* Format: BNG  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* T    09/09/1994 20:03:49  SE    12345    67890                           */
/* 0    1	             2     3        4                               */
/* TYPE DATE/TIME            ZONE  EASTING  NORTHING                        */
/*                                                                          */
/*                                                                          */
/* Format: ITM  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* T    09/09/1994 20:03:49  IN    12345    67890                           */
/* 0    1	             2     3        4                               */
/* TYPE DATE/TIME            ZONE  EASTING  NORTHING                        */
/*                                                                          */
/*                                                                          */
/* Format: SUI  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS             */
/* T    09/09/1994 20:03:49  012345   067890                                */
/* 0    1	             3        4                                     */
/* TYPE DATE/TIME            EASTING  NORTHING                              */
/*                                                                          */
/* Format: SEG  UTC Offset:   1.00h   Datum[085]: RT90                      */
/* T    09/09/1994 20:03:49  012345   067890                                */
/* 0    1	             2        3                                     */
/* TYPE DATE/TIME            EASTING  NORTHING                              */
/*                                                                          */
/*                                                                          */
/* Format: GKK  UTC Offset:   1.00 hrs  Datum[102]: Potsdam                 */
/* T    07/28/1994 20:12:54  0123456  0678901                               */
/* 0    1                    2        3                                     */
/* TYPE DATE/TIME            EASTING  NORTHING                              */
/*                                                                          */
/*                                                                          */
/* Note: Even though the date/time is included in the protocol, the GPS 45  */
/* appears to ignore this field. The only track data that is time stamped   */
/* is that actually generated by the GPS 45; actual track data uploaded     */
/* from the GPS 45 has correct time stamps.                                 */
/*                                                                          */
/****************************************************************************/

/****************************************************************************/
/* Process Track package - return 0 if GPS does not respond.                */
/****************************************************************************/
static short
doTrack ()
{
  double latitude, longitude, x, y;
  long lat, lon;
  short zone;
  char nsZone, bgzone[3], finzone[5];
  int pos;

  enum { LAT, LON };
  enum { ZE, ZN, EASTING, NORTHING }; /* UTM Grid */
  enum { BGZ, BGEAST, BGNORTH }; /* British, Irish Grid */
  enum { SEAST, SNORTH };	/* Swiss Grid */
  enum { SEGEAST, SEGNORTH };	/* Swedish Grid */
				/* same format for German Grid */

  struct 
  {
    char type;
    char *comment, *name;    
    long seconds;
    double latitude, longitude;
    double altitude;
    struct {
      /* latitude and longitude are always present */
      unsigned altitude:1;
      unsigned seconds:1;
      unsigned name:1;
      unsigned comment:1;
    } has;
  } pkt;
  char empty[]="";

  pkt.has.altitude =
    pkt.has.seconds =
    pkt.has.name =
    pkt.has.comment = 0;

  pkt.altitude = 1.01e24;
  pkt.seconds = ~0;
  pkt.comment = pkt.name = empty;

  if (column.date >= 0)
    {
      char date[20];
      strncpy (date, field (fileData, column.date), 19);
      date[19] = 0;
      pkt.seconds = dt2secs (date, gFilePrefs.offset);
      pkt.has.seconds = 1;
    }
  if ((column.altitude >= 0) && 
      (!strstr(field(fileData, column.altitude), "unknown")))
    {
      pkt.altitude = atof (field (fileData, column.altitude));
      pkt.has.altitude = 1;
    }

  pos=column.position;

  /* handle various grids */
  switch (gFilePrefs.format)
    {
    case DMS:
      cleanupDMS (fileData);
      latitude = DMStoDegrees (field (fileData, pos+LAT));
      longitude = DMStoDegrees (field (fileData, pos+LON));
      break;

    case DMM:
      cleanupDMS (fileData);
      latitude = DMtoDegrees (field (fileData, pos+LAT));
      longitude = DMtoDegrees (field (fileData, pos+LON));
      break;

    case DDD:
      sscanf (field (fileData, pos+LAT), "%lf %lf", &latitude, &longitude);
      break;

    case UTM:			/* UTM Grid */
      sscanf (field (fileData, pos+ZE), "%hd", &zone);
      sscanf (field (fileData, pos+ZN), "%c", &nsZone);
      sscanf (field (fileData, pos+EASTING), "%lf %lf", &x, &y);
      UTMtoDeg (zone, nsZone <= 'M', x, y, &latitude, &longitude);
      break;

    case KKJ:			/* KKJ Grid */
      sscanf (field (fileData, pos+ZE), "%s", finzone);
      sscanf (field (fileData, pos+EASTING), "%lf %lf", &x, &y);
      KKJtoDeg (2, 0, x, y, &latitude, &longitude);	/* 2 is 27E, not used currently */
      break;

    case BNG:			/* British Grid */
      sscanf (field (fileData, pos+BGZ), "%s", bgzone);
      sscanf (field (fileData, pos+BGEAST), "%lf %lf", &x, &y);
      BNGtoDeg (bgzone, x, y, &latitude, &longitude);
      break;

    case ITM:			/* Irish Grid */
      sscanf (field (fileData, pos+BGZ), "%s", bgzone);
      sscanf (field (fileData, pos+BGEAST), "%lf %lf", &x, &y);
      ITMtoDeg (bgzone, x, y, &latitude, &longitude);
      break;

    case SEG:			/* Swedish Grid */
      sscanf (field (fileData, pos+SEGEAST), "%lf %lf", &x, &y);
      SEGtoDeg (x, y, &latitude, &longitude);
      break;

    case GKK:			/* German Grid */
      sscanf (field (fileData, pos+SEGEAST), "%lf %lf", &x, &y);
      GKKtoDeg (x, y, &latitude, &longitude);
      break;

    default:
      break;
    }

  /* translate to selected datum */
  translate (0, &latitude, &longitude, gFilePrefs.datum);
  pkt.latitude = latitude;
  pkt.longitude = longitude;

  /* convert numbers to GPS bytes */
  lat = deg2int (latitude);
  lon = deg2int (longitude);

  /* create package header */
  message[0] = Pid_Trk_Data;

#ifdef OLD
  /* number of message bytes to follow */
  message[1] = 13;

  /* copy position and time to GPS package */
  copyNumber (message + 2, lat);
  copyNumber (message + 6, lon);
  copyNumber (message + 10, dat);
  /* set "new track flag" */
#endif 
  
  if (strstr(protocols, "D300"))
    {
      D300_Trk_Point_Type *d=(D300_Trk_Point_Type *)(message+2);
      copyNumber ((BYTE *)&d->time, pkt.seconds);
      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      d->new_trk = newTrack;
      message[1] = sizeof(*d);
    }
  if (strstr(protocols, "D301"))
    {
      D301_Trk_Point_Type *d=(D301_Trk_Point_Type *)(message+2);
      copyNumber ((BYTE *)&d->time, pkt.seconds);
      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      if (pkt.has.altitude)
	copyFloat ((BYTE *)&d->alt, pkt.altitude);
       d->new_trk = newTrack;
      message[1] = sizeof(*d);
    }
  if (strstr(protocols, "D302"))
    {
      D302_Trk_Point_Type *d=(D302_Trk_Point_Type *)(message+2);
      copyNumber ((BYTE *)&d->time, pkt.seconds);
      copyNumber ((BYTE *)&d->posn.lat, deg2int (pkt.latitude));
      copyNumber ((BYTE *)&d->posn.lon, deg2int (pkt.longitude));
      if (pkt.has.altitude)
	copyFloat ((BYTE *)&d->alt, pkt.altitude);
      d->new_trk = newTrack;
      message[1] = sizeof(*d);
    }
  
  newTrack = 0;

  /* send package to GPS */
  sendGPSMessage (message, message[1]+2);

  /* make sure GPS responds */
  return getGPSMessage ();
}


/****************************************************************************/
/* Route file format:                                                       */
/*                                                                          */
/*                                                                          */
/* R 	1	BOGUS 1                                                     */
/* 0    1       2                                                           */
/*                                                                          */
/****************************************************************************/

/****************************************************************************/
/* Process Route package - return 0 if GPS does not respond.                */
/****************************************************************************/
static short
doRoute ()
{
  short route;

  /* create package header */
  message[0] = '\x1d';

  /* number of message bytes to follow */
  message[1] = 21;

  /* copy route data to GPS package */
  sscanf (field (fileData, 1), "%hd", &route);
//  route = (short) strtol(field(fileData, 1), (char **)NULL, 10);
  message[2] = route;
  cpystr (message + 3, field (fileData, 2), 20);

  /* send package to GPS */
  sendGPSMessage (message, 23);

  /* make sure GPS responds */
  return getGPSMessage ();
}


/****************************************************************************/
/* Process Almanac package - uploading almanac data is no longer supported. */
/****************************************************************************/
#define ALM_BYTES 42

static short
doAlmanac ()
{
  short i;
  unsigned short x;

  /* Print error message and return */
  fprintf (stderr,
	   "Error: Uploading of almanac data is no longer supported.\n");
  return (0);


  /* create package header */
  message[0] = '\x1f';

  /* number of message bytes to follow */
  message[1] = ALM_BYTES;

  /* copy almanac bytes to GPS package */
  for (i = 0; i < ALM_BYTES; i++)
    {
      sscanf (fileData + 2 + 3 * i, "%02hX", &x);
      message[i + 2] = (BYTE) x;
    }

  /* send package to GPS */
  sendGPSMessage (message, ALM_BYTES + 2);

  /* make sure GPS respond */
  return getGPSMessage ();
}


/****************************************************************************/
/* Return number of records in input-file.                                  */
/****************************************************************************/
static short
records (FILE *ifile, short type)
{
  short n = 0;

  /* Get line from input file */
  GetLine (ifile, fileData, 1);

  /* Parse package type until end-of-file */
  while (GetLine (ifile, fileData, 0))
    {
      if (strncasecmp(fileData, "Type", 4))
	{
	  /* process different types */
	  switch (type)
	    {
	    case ALMANAC:
	      if (fileData[0] == 'A')
		++n;
	      break;
	      
	    case ROUTE:
	      if (fileData[0] == 'R' || fileData[0] == 'W')
		++n;
	      break;
	      
	    case TRACK:
	      if (fileData[0] == 'T')
		++n;
	      break;
	      
	    case WAYPOINT:
	      if (fileData[0] == 'W')
		++n;
	      break;
	    default:
	      /* ignore blank lines, comments, etc. */
	      break;
	    }
	}
    }

  /* return number of records */
  return n;
}


/****************************************************************************/
/* Send a message containing the number of records to follow. Returns a     */
/* result code - zero if GPS did not respond.                               */
/****************************************************************************/
static short
sendRecords (short recs)
{
  short result;

  /* create package header */
  message[0] = '\x1b';

  /* number of message bytes to follow */
  message[1] = '\x02';

  /* convert number of records to GPS bytes */
  message[2] = recs % 256;	/* LSB of number of records */
  message[3] = recs / 256;	/* MSB */

  /* send package to GPS */
  sendGPSMessage (message, 4);

  /* make sure GPS responded */
  result = getGPSMessage ();
  return result;
}


/****************************************************************************/
/* Convert long number to bytes.                                            */
/****************************************************************************/
static void
copyNumber (BYTE * p, long n)
{
  *p++ = n & 0xff;		/* LSB first */
  n >>= 8;
  *p++ = n & 0xff;
  n >>= 8;
  *p++ = n & 0xff;
  n >>= 8;
  *p = n & 0xff;
}


/****************************************************************************/
/* Convert float number to bytes.                                           */
/****************************************************************************/
static void
copyFloat (BYTE * p, double d)
{
  float f = (float)d;
  long *lp = (long *) &f;
  long n = *lp;

  *p++ = n & 0xff;		/* LSB first */
  n >>= 8;
  *p++ = n & 0xff;
  n >>= 8;
  *p++ = n & 0xff;
  n >>= 8;
  *p = n & 0xff;
}


/****************************************************************************/
/* returns a pointer to the nth field of a tab-delimited record which       */
/* starts at address a.                                                     */
/****************************************************************************/
static char *
field (char *str, short n)
{
  short i = 0;
  char *a = str;

  while ((i++) < n)
    do
      {
	if (!*a)
	  {
	    char buf[160];
	    sprintf(buf, "Input line should have at least %d tabs:\n%80s\n", 
		    n-1, str);
	    Error(buf);
	  }
      } while (*(++a) != '\t');

  return (a + 1);
}


/****************************************************************************/
/* Copy a string to GPS bytes.                                              */
/****************************************************************************/
static void
cpystr (BYTE * p, char *q, short n)
{
  while ((*q != '\t') && (*q != '\n') && (*q != '\r') && (*q != 0))
    {
      *p++ = *q++;
      --n;
    }
  while (n-- > 0)
    *p++ = ' ';
}


/****************************************************************************/
/*                                                                          */
/*  The following function retrieves the file format info from the first    */
/*  lines of the data file. This is highly dependent upon the following     */
/*  format for the first line in the file -- note that there are no         */
/*  embedded tabs. See the file "getGPSInfo.c" for further information.     */
/*                                                                          */
/*  Format: DDD  UTC Offset:  -6.00 hrs  Datum[061]: NAD27 CONUS            */
/*          |||              ||||||            |||                          */
/*  0         1         2         3         4         5         6           */
/*  0123456789012345678901234567890123456789012345678901234567890123456789  */
/*                                                                          */
/****************************************************************************/
/*
  for Mayko track:
xmaplog 1.0 Sat Apr  9 15:37:06 2005

1 24.9745041 121.5487808 0.0 0 12/11/1996 17:20:08
1 24.9745792 121.5483677 1.7 0 12/11/1996 17:20:58

  for Mayko track type 2, "-m -m".  Blank lines mark breaks in the track (GPS was off)
xmaplog 1.0 Wed May 11 13:25:08 2005

1 24.9745041 121.5487808 0.0 0 12/11/1996 17:20:08
1 24.9745792 121.5483677 1.7 0 12/11/1996 17:20:58 x24.9745041 121.5487808 0.0424647 -50- 1.651

  for Mayko route:
xmaproute 1.0 Sun May  8 15:31:45 2005

  for Mayko waypoints:
unknown xmap format Sun May  8 15:30:53 2005
W       001             67.657959 meters        12/31/1989 -5:00:00     42.9273394      -71.4672483

*/

/****************************************************************************/
/* Get data format from the first two lines of the input file. Return
   nonzero on success.  */
/****************************************************************************/
static short
getFileFormat (char *first, char *second)
{
  extern short nDatums;
  char fmt[10];

  
  if (strstr(first, "xmap"))
    {				// manage Mayko mXmap format
      file_format = MAYKO;
      gFilePrefs.format = DDD;
      gFilePrefs.offset = 0;
      gFilePrefs.datum = 100;
      if (verbose)
	printf("Input file is in Mayko mXmap format.\n");
    }
  else
    {
      sscanf (first, "%7s", fmt);
      if (strcmp (fmt, "Format:") != 0)
	return 0;

      /* Get position format */
      sscanf (first + 8, "%3s", fmt);
      if (strcmp (fmt, "DMS") == 0)
	gFilePrefs.format = DMS;
      else if (strcmp (fmt, "DMM") == 0)
	gFilePrefs.format = DMM;
      else if (strcmp (fmt, "DDD") == 0)
	gFilePrefs.format = DDD;
      else if (strcmp (fmt, "UTM") == 0)
	gFilePrefs.format = UTM;
      else if (strcmp (fmt, "BNG") == 0)
	gFilePrefs.format = BNG;
      else if (strcmp (fmt, "ITM") == 0)
	gFilePrefs.format = ITM;
      else if (strcmp (fmt, "KKJ") == 0)
	gFilePrefs.format = KKJ;
      else if (strcmp (fmt, "SEG") == 0)
	gFilePrefs.format = SEG;
      else if (strcmp (fmt, "GKK") == 0)
	gFilePrefs.format = GKK;
      else
	return 0;

      /* Get time offset */
      sscanf (first + 25, "%lf", &gFilePrefs.offset);
      if (gFilePrefs.offset < -24.0 || gFilePrefs.offset > 24.0)
	return 0;

      /* Get datum format */
      sscanf (first + 43, "%3hd", &gFilePrefs.datum);
      /* nDatums - 1 was wrong, as Datums count from 0 to 102, that are 103 Datums,
         but nDatums was set to 102 */
      if (gFilePrefs.datum < 0 || gFilePrefs.datum > nDatums)
	return 0;

      if (!*second)
	return 0;

      {
	int i;
	char *s;

	column.count=0;
	/* count the column labels.  Labels are separated by tabs.  A
	   tab is optional after the last label. */
	for (s=second; *s; s++)
	  if ((*s == '\t') && s[1])
	    column.count++;

	s=second;
	for (i=0; i<column.count; i++)
	  {
	    if (     strncasecmp(s, "type",     strlen("type"))==0)
	      column.type = i;
	    else if (strncasecmp(s, "name",     strlen("name"))==0)
	      column.name = i;
	    else if (strncasecmp(s, "comment",  strlen("comment"))==0)
	      column.comment = i;
	    else if (strncasecmp(s, "date",     strlen("date"))==0)
	      column.date = i;
	    else if (strncasecmp(s, "altitude", strlen("altitude"))==0)
	      column.altitude = i;
	    else if ((strncasecmp(s, "latitude", strlen("latitude"))==0) && 
		     ((gPrefs.format==DMS) || 
		      (gPrefs.format==DMM) ||
		      (gPrefs.format==DDD)))
	      column.position = i;
	    else if ((strncasecmp(s, "ze", strlen("ze"))==0) && 
		     (gPrefs.format==UTM))
	      column.position = i;
	    else if ((strncasecmp(s, "zone", strlen("zone"))==0) && 
		     ((gPrefs.format==BNG) || 
		      (gPrefs.format==ITM) || 
		      (gPrefs.format==KKJ)))
	      column.position = i;
	    else if ((strncasecmp(s, "easting", strlen("easting"))==0) && 
		     ((gPrefs.format==SEG) || 
		   /* (gPrefs.format==SUI) ||  not implemented */
		      (gPrefs.format==GKK)))
	      column.position = i;
	    while (*s++ != '\t')
	      ;
	  }
	}
    }

  /* Return if everything is okay */
  return 1;
}


/****************************************************************************/
/* Change degree, minutes and second symbols to spaces.                     */
/****************************************************************************/
static void
cleanupDMS (char *s)
{
  while (*++s)
    if (*s == '�' || *s == '\260' || *s == '\'' || *s == '\"')
      *s = ' ';
}
