/****************************************************************************/
/*                                                                          */
/* ./gps/garmincomm.c   -   Basic communication procedures between gps and  */
/*                          unix host.                                      */
/*                                                                          */
/* This file is part of gpstrans - a program to communicate with garmin gps */
/* Parts are taken from John F. Waers (jfwaers@csn.net) program MacGPS.     */
/*                                                                          */
/*                                                                          */
/*    Copyright (c) 1995 by Carsten Tschach (tschach@zedat.fu-berlin.de)    */
/*                                                                          */
/*    Copyright (c) 2001 by Joao Seabra - CT2GNL (seabra@ci.aac.uc.pt)      */
/*                                                                          */
/*                                                                          */
/* This program is free software; you can redistribute it and/or            */
/* modify it under the terms of the GNU General Public License              */
/* as published by the Free Software Foundation; either version 2           */
/* of the License, or (at your option) any later version.                   */
/*                                                                          */
/* This program is distributed in the hope that it will be useful,          */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of           */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            */
/* GNU General Public License for more details.                             */
/*                                                                          */
/* You should have received a copy of the GNU General Public License        */
/* along with this program; if not, write to the Free Software              */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,   */
/* USA.                                                                     */
/****************************************************************************/


#include "defs.h"
#include "Garmin.h"
#include "Prefs.h"
#include "util.h"
#include <assert.h>

/* declare external variables */
extern int ttyfp;

/* define global variables */
BYTE gGarminMessage[MAX_LENGTH];
int naks;

/* define static variables */
static unsigned char checksum;


/****************************************************************************/
/* Prepare package and send to GPS.                                         */
/*                                                                          */
/* A Garmin packet has six bytes of overhead.  The caller includes two      */
/* bytes of the packet header (ID and data byte count) in the message.      */
/* This routine prepends the DLE and appends the trailer (checksum, DLE,    */
/* and ETX).                                                                */
/****************************************************************************/
void
sendGPSMessage (BYTE * message, short bytes)
{
  short i;
  long n;
  BYTE gGarminMessage[MAX_LENGTH];
  BYTE *p = gGarminMessage;

  checksum=0;
  *p++ = 0x10;			/* Define start of message */

  for (i = 0; i < bytes; i++)
    {
      *p++ = message[i];
      checksum -= message[i];
      if (message[i] == 0x10)
	*p++ = 0x10;
    }

  *p++ = checksum;
  if (checksum == 0x10)
    *p++ = 0x10;
  *p++ = 0x10;
  *p++ = 0x03;
  n = (long) (p - gGarminMessage);

  if (debugging>1)
    {
      printf ("  Sending  : ");
      for (i = 0; i < n; i++)
	printf ("%02X ", *(gGarminMessage + i));
      printf ("\n");
      if ((bytes != (message[1]+2)) || (bytes > 254))
	Error("sendGPSMessage: invalid byte count\n");
    }

  write (ttyfp, gGarminMessage, n);
}


/****************************************************************************/
/* Sent a "are-you-ready" message to GPS - return TRUE if GPS responds or   */
/* FALSE if GPS did not respond (GPS off? / incorrect format?)              */
/****************************************************************************/
int
CheckGPS ()
{
  extern struct PREFS gPrefs;
  extern char gMessageStr[];

  BYTE *p = gGarminMessage;
  BYTE last = 0;
  BYTE c;
  short err;
  int igot;
  short length = 0;

  long tttime = TickCount ();


  /* Open device, FALSE if unable to open device */
  if ((err = serialOpen (GARMIN)) != noErr)
    {
      sprintf (gMessageStr, "The initialization of port %s has failed.",
	       gPrefs.Device);
      Error (gMessageStr);
      serialClose ();
      return (1 == 0);
    }

  if (debugging)
    fprintf(stderr, "CheckGPS: sending test packet\n");

  sendGPSMessage (test, 4);
  /* The "test" message has packet ID 0x1c=28, which is not listed
     in Garmin's ICD.  Apparently it is an invalid ID, so the device
     will reply with an ACK but discard the packet. */

  /* wait for response */
  for (;;)
    {

      /* timeout exceeded */
      if (TickCount () > (tttime + (long) TIMEOUT))
	{
	  serialClose ();
	  return (1 == 0);
	}

      while (serialCharsAvail ())
	{
	  igot = read (ttyfp, &c, 1);

	  if (c == 0x10 && last == 0x10)
	    last = 0;
	  else
	    {
	      assert(p < gGarminMessage + MAX_LENGTH);
	      last = *p++ = c;
	      ++length;
	    }

	  /* received valid response package */
	  if (*(p - 1) == 0x03 && *(p - 2) == 0x10)
	    {
	      if (debugging>1)
		{
		  int i;
		  printf ("  Receiving:   ");
		  for (i = 0; i < length; i++)
		    printf ("%02X ", *(gGarminMessage + i));
		  printf ("\n");
		}
	      serialClose ();
	      return (1 == 1);
	    }
	}
    }
}


/****************************************************************************/
/* Get response from GPS - returns number of bytes received, 0 if timeout    */
/* period elapsed without completing a package.                             */
/****************************************************************************/
short
getGPSMessage ()
{
  extern enum PROTOCOL mode;

  BYTE *p = gGarminMessage;
  BYTE last = 0;
  BYTE c;
  short length = 0;
  int igot;
  long tttime = TickCount ();

  checksum = -(0x10 + 0x03 + 0x10); /* with this starting value, the
					sum over the entire packet
					should be zero */


  for (;;)
    {

      /* exit if timeout exceeded */
      if (TickCount () > (tttime + (long) TIMEOUT))
	{
	  CloseBarGraph ();
	  Error ("The GPS receiver is not responding.");
	  mode = NONE;
	  return 0;
	}

      while (serialCharsAvail ())
	{
	  if (length >= MAX_LENGTH - 1)
	    {
	      Error ("GPS receiver communication protocol error.");
	      mode = NONE;
	      return 0;
	    }

	  igot = read (ttyfp, &c, 1);

	  if (c == 0x10 && last == 0x10)
	    last = 0;
	  else
	    {
	      assert(p < gGarminMessage + MAX_LENGTH);
	      last = *p++ = c;
	      checksum += c;
	      ++length;
	    }

	  /* return when package complete */
	  if (*(p - 1) == 0x03 && *(p - 2) == 0x10)
	    {
	      if (debugging>1)
		{
		  int i;
		  printf ("  Receiving:   ");
		  for (i = 0; i < length; i++)
		    printf ("%02X ", *(gGarminMessage + i));
		  printf ("\n");
		  if (debugging && checksum)
		    printf("getGPSMessage: ########  checksum=%d is nonzero  #########\n",
			   checksum);
		}

	      return length;
	    }
	}
    }
}

/* Read the ACK packet from the GPS.  Sometimes we get an invalid
   packet "00 F5 10 03" instead, which we ignore.  Return number of
   bytes in the valid ACK packet, or zero on failure. */
int
getGPSack ()
{
  int n, trial;
  for (trial=0; trial<3; trial++)
    {
      n = getGPSMessage();		
      if ((n<8) ||
	  (gGarminMessage[0]!=0x10) ||
	  (gGarminMessage[2]!=n-6) ||
	  checksum || 
	  (gGarminMessage[n-2]!=0x10) ||
	  (gGarminMessage[n-1]!=0x03)
	  )
	{
	  if (debugging)
	    fprintf(stderr, "GPSack: discarding invalid packet\n");
	  continue;
	}
      if (gGarminMessage[1]==0x06)	/* is the packet an ACK? */
	return n;  
      if (gGarminMessage[1]==21)	/* is the packet a NAK? */
	{
	  naks++;
	  return 0;
	}
      if (debugging)
	fprintf(stderr, "GPSack: discarding packet type 0x%02X=%d, "
		"neither ACK nor NAK\n",
		gGarminMessage[1], gGarminMessage[1]);
    }
  return 0;
}
